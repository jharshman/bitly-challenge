.PHONY: fmt build gencerts run_server run_client check_fmt release protobuf mockgen clean

default: build

fmt:
	./build-tools/scripts/check --fmt

build:
	./build-tools/scripts/build -p 3

gencerts:
	./build-tools/scripts/gencerts

run_server: gencerts
	./bin/bitly-challenge_darwin_amd64 server --loglvl debug --port 9000 --cert ./bin/tls/cert.pem --key ./bin/tls/key.pem

run_client:
ifeq ($(BITLY_API_TOKEN),)
	$(error missing api token, please set BITLY_API_TOKEN)
endif
	./bin/bitly-challenge_darwin_amd64 client --client-cert ./bin/tls/cert.pem --api-token $(BITLY_API_TOKEN)

run_curl:
ifeq ($(BITLY_API_TOKEN),)
	$(error missing api token, please set BITLY_API_TOKEN)
endif
	curl -H "Authorization: Bearer $(BITLY_API_TOKEN)" http://127.0.0.1:8080/average

check_fmt:
	./build-tools/scripts/check

release:
	./build-tools/scripts/release $(DOCKER_USER) $(DOCKER_PASSWORD)

protobuf:
	protoc -I/usr/local/include \
		-I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--proto_path=pkg/grpc \
		--go_out=plugins=grpc:pkg/grpc \
		clicktraffic.proto
	protoc -I/usr/local/include \
		-I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--proto_path=pkg/grpc \
		--grpc-gateway_out=logtostderr=true:pkg/grpc \
		clicktraffic.proto

mockgen:
	mockgen --source=pkg/grpc/clicktraffic.pb.go --destination=mock/clicktraffic.go --package=mock

clean:
	rm -rf ./bin/*
