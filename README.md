# bitly-challenge
Client & Server implementation with gRPC HTTP gateway.

## Run with Docker
You can use the Pre-built image, just remember to generate some certs first. Either specify your own or use `make gencerts`.
```
$ docker run -d --rm --name bitly-server -v $PWD/bin/tls:/tls jharshman/bitly-challenge:latest server --loglvl debug --cert /tls/cert.pem --key /tls/key.pem

$ docker run --rm -v $PWD/bin/tls:/tls --link=bitly-server:bitly-challenge.example.int jharshman/bitly-challenge:latest client --client-cert /tls/cert.pem --server bitly-challenge.example.int --api-token ${BITLY_API_TOKEN}
```

## Build
```
$ make build
```

## Run the server
```
$ make run_server
```

## Run the client
```
$ BITLY_API_TOKEN=sometoken make run_client
```

## cURL
```
$ BITLY_API_TOKEN=sometoken make run_curl
```

# Component Breakdown
## Server Usage
```
➜  bitly-challenge ./bin/bitly-challenge_darwin_amd64 server --help                                                                           (rnd/kube-system)
Run the bitly server

Usage:
  bitly-challenge server [flags]

Flags:
      --api-url string   Bitly api url (default "https://api-ssl.bitly.com/v4")
      --cert string      tls cert (default "cert.pem")
  -h, --help             help for server
      --key string       tls key (default "key.pem")
      --loglvl string    log level (default "info")
      --port string      grpc bind address (default "9000")
```

## Client Usage
```
➜  bitly-challenge ./bin/bitly-challenge_darwin_amd64 client --help                                                                           (rnd/kube-system)

Facilitates communication to Bitly Server.

Usage:
  bitly-challenge client [flags]

Flags:
      --api-token string     bitly API token
      --client-cert string   client tls certificate (default "cert.pem")
  -h, --help                 help for client
      --server string        address of echo server (default "127.0.0.1")
      --server-port string   port of echo server (default "9000")
```