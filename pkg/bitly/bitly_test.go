package bitly

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/matryer/is"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestClient_UserDefaultGroup(t *testing.T) {

	r := chi.NewRouter()
	testServe := httptest.NewServer(r)
	defer testServe.Close()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.HandleFunc("/user", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusOK)
		writer.Write([]byte(`{
  "created": "2019-04-14T23:48:18+0000",
  "modified": "2019-04-14T23:48:18+0000",
  "login": "jharshman",
  "is_active": true,
  "is_2fa_enabled": false,
  "name": "jharshman",
  "emails": [
    {
      "email": "joshua@joshuaharshman.com",
      "is_primary": true,
      "is_verified": true
    }
  ],
  "is_sso_user": false,
  "default_group_guid": "Bj4enuqQei6"
}`))
	})

	cli := &Client{
		Cli: &http.Client{},
		Url: testServe.URL,
	}

	is := is.New(t)
	guid, err := cli.UserDefaultGroup("sometoken")
	is.NoErr(err)
	is.Equal(guid, "Bj4enuqQei6")
}

func TestClient_Bitlinks(t *testing.T) {
	r := chi.NewRouter()
	testServe := httptest.NewServer(r)
	defer testServe.Close()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.HandleFunc("/groups/{group_id}/bitlinks", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusOK)
		writer.Write([]byte(`{
  "links": [
    {
      "created_at": "2019-04-15T17:10:31+0000",
      "id": "bit.ly/2XdKl26",
      "link": "http://bit.ly/2XdKl26",
      "custom_bitlinks": [],
      "long_url": "https://gist.github.com/jctbitly/05044bb3281ca6723bc118babc77afc7",
      "title": "bitly_backend_test.md · GitHub",
      "archived": false,
      "created_by": "jharshman",
      "client_id": "ece654beaf35f9c29f610ffd4fb128702b4bad15",
      "tags": [],
      "deeplinks": [],
      "references": {
        "group": "https://api-ssl.bitly.com/v4/groups/Bj4enuqQei6"
      }
    }
  ],
  "pagination": {
    "prev": "",
    "next": "",
    "size": 50,
    "page": 1,
    "total": 1
  }
}`))
	})

	cli := &Client{
		Cli: &http.Client{},
		Url: testServe.URL,
	}
	is := is.New(t)
	links, err := cli.Bitlinks("sometoken", "Bj4enuqQei6")
	is.NoErr(err)
	is.Equal(links, []string{"bit.ly/2XdKl26"})
}

func TestClient_ClicksByCountry(t *testing.T) {
	r := chi.NewRouter()
	testServe := httptest.NewServer(r)
	defer testServe.Close()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.HandleFunc("/bitlinks/{bitlink}/countries", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusOK)
		writer.Write([]byte(`{
  "unit_reference": "2019-04-16T16:22:39+0000",
  "metrics": [
    {
      "value": "US",
      "clicks": 1
    }
  ],
  "units": -1,
  "unit": "day",
  "facet": "countries"
}`))
	})

	cli := &Client{
		Cli: &http.Client{},
		Url: testServe.URL,
	}
	is := is.New(t)
	clicks, err := cli.ClicksByCountry("sometoken", "somelink", 30)
	is.NoErr(err)
	is.Equal(clicks, uint64(1))
}
