package bitly

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

type Client struct {
	Cli *http.Client
	Url string
}

// todo: define some response types
type UserInfo struct {
	Name         string `json:"name"`
	DefaultGroup string `json:"default_group_guid"`
}

type Bitlinks struct {
	Links []struct {
		Id string `json:"id"`
	} `json:"links"`
	Pg struct {
		Prev  string `json:"prev"`
		Next  string `json:"next"`
		Size  int64  `json:"size"`
		Page  int64  `json:"page"`
		Total int64  `json:"total"`
	} `json:"pagination"`
}

type ClickMetrics struct {
	Metrics []struct {
		Country string `json:"value"`
		Clicks  uint64 `json:"clicks"`
	} `json:"metrics"`
}

// todo: get user default group https://api-ssl.bitly.com/v4/user
func (c *Client) UserDefaultGroup(token string) (string, error) {
	req := createRequest(token, "%s/user", c.Url)
	resp, err := c.Cli.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	u := &UserInfo{}
	err = json.NewDecoder(resp.Body).Decode(u)
	if err != nil {
		return "", err
	}
	return u.DefaultGroup, nil
}

//
//// todo: get number bitlinks https://api-ssl.bitly.com/v4/groups/{group_guid}/bitlinks
func (c *Client) Bitlinks(token, guid string) ([]string, error) {
	var links []string
	page := ""
	for {
		bl, err := c.pager(createRequest(token, "%s/groups/%s/bitlinks", c.Url, guid), page)
		if err != nil {
			return nil, err
		}
		for _, v := range bl.Links {
			links = append(links, v.Id)
		}
		page = bl.Pg.Next
		if page == "" {
			break
		}
	}
	return links, nil
}

func (c *Client) ClicksByCountry(token, link string, duration int64) (uint64, error) {
	req := createRequest(token, "%s/bitlinks/%s/countries", c.Url, link)
	qv := req.URL.Query()
	qv.Set("units", strconv.FormatInt(duration, 10))
	req.URL.RawQuery = qv.Encode()
	resp, err := c.Cli.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	cm := &ClickMetrics{}
	err = json.NewDecoder(resp.Body).Decode(cm)
	if err != nil {
		return 0, err
	}
	clickCounter := uint64(0)
	for _, v := range cm.Metrics {
		clickCounter += v.Clicks
	}
	return clickCounter, nil
}

func (c *Client) pager(r *http.Request, page string) (*Bitlinks, error) {
	if page != "" {
		qv := r.URL.Query()
		qv.Set("page", page)
		r.URL.RawQuery = qv.Encode()
	}
	resp, err := c.Cli.Do(r)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	l := &Bitlinks{}
	err = json.NewDecoder(resp.Body).Decode(l)
	if err != nil {
		return &Bitlinks{}, err
	}
	return l, nil
}

func createRequest(token, format string, args ...interface{}) *http.Request {
	req, _ := http.NewRequest("GET", fmt.Sprintf(format, args...), nil)
	req.Header.Set("authorization", fmt.Sprintf("Bearer %s", token))
	return req
}

//
//// todo: get clicks by country https://api-ssl.bitly.com/v4/bitlinks/{bitlink}/countries
//func (c *Client) ClicksByCountry() error {}
