package server

import (
	"context"
	"github.com/jharshman/bitly-challenge/pkg/bitly"
	"github.com/jharshman/bitly-challenge/pkg/grpc"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type handler struct {
	bc *bitly.Client
}

// Echo logs received message and returns it.
func (h *handler) Average(ctx context.Context, in *grpc.Request) (*grpc.Avg, error) {
	token := ctx.Value(contextKeyAuthToken)
	group, err := h.bc.UserDefaultGroup(token.(string))
	if err != nil {
		log.Errorf("getting user default group: %v\n", err)
		return &grpc.Avg{}, nil
	}
	log.Debugf("got guid: %s\n", group)

	links, err := h.bc.Bitlinks(token.(string), group)
	if err != nil {
		log.Errorf("getting links: %v\n", err)
		return &grpc.Avg{}, nil
	}
	if links == nil {
		log.Errorf("found no links")
		return &grpc.Avg{}, status.Errorf(codes.NotFound, "found no links")
	}
	log.Infof("%+v\n", links)
	var totalClicks uint64
	for _, v := range links {
		c, err := h.bc.ClicksByCountry(token.(string), v, in.Time)
		if err != nil {
			log.Errorf("getting num clicks for %s: %v\n", v, err)
			return &grpc.Avg{}, status.Errorf(codes.Internal, "getting num clicks for %s: %v", v, err)
		}
		totalClicks += c
	}
	log.Infof("total clicks for all links in all countries: %d\n", totalClicks)
	avg := float32(totalClicks) / float32(len(links))
	log.Infof("average clicks across all clicked links in all countries: %d\n", avg)
	return &grpc.Avg{
		Time:    in.Time,
		Average: avg,
	}, nil
}
