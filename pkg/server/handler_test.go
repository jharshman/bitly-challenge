package server

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/jharshman/bitly-challenge/mock"
	"github.com/jharshman/bitly-challenge/pkg/grpc"
	"github.com/matryer/is"
	"testing"
)

func TestHandler_Average(t *testing.T) {
	is := is.New(t)
	mctl := gomock.NewController(t)
	defer mctl.Finish()

	mcli := mock.NewMockClickTrafficServer(mctl)
	mcli.EXPECT().Average(gomock.Any(), gomock.Any()).Return(&grpc.Avg{
		Average: 10,
	}, nil)

	res, err := mcli.Average(context.Background(), &grpc.Request{
		Time: 30,
	})

	is.NoErr(err)
	is.Equal(res.Average, uint64(10))
}
