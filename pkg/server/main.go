package server

import (
	"context"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/jharshman/bitly-challenge/config"
	"github.com/jharshman/bitly-challenge/pkg/bitly"
	"github.com/jharshman/bitly-challenge/pkg/grpc"
	log "github.com/sirupsen/logrus"
	rpc "google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// Start initializes the gRPC server process.
func Start(cfg *config.Server) error {
	initLogger(cfg.LogLevel())

	//debug logging
	log.Debugf("port %s\n", cfg.Port)
	log.Debugf("log level %s\n", cfg.LogLvl)
	log.Debugf("cert file %s\n", cfg.CertFile)
	log.Debugf("key file %s\n", cfg.KeyFile)

	lis, err := net.Listen("tcp", cfg.BindPort())
	if err != nil {
		return err
	}
	defer lis.Close()

	tls, _ := credentials.NewServerTLSFromFile(cfg.CertFile, cfg.KeyFile)
	opts := []rpc.ServerOption{
		rpc.Creds(tls),
		rpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(grpc_auth.UnaryServerInterceptor(auth))),
	}
	grpcServer := rpc.NewServer(opts...)

	srv := &handler{
		bc: &bitly.Client{
			Cli: http.DefaultClient,
			Url: cfg.ApiUrl,
		},
	}
	grpc.RegisterClickTrafficServer(grpcServer, srv)

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM)

	log.Infof("starting grpc server: %s\n", cfg.BindPort())
	go grpcServer.Serve(lis)
	go startGw(cfg)

	<-done
	grpcServer.Stop()
	log.Infof("stopped server\n")

	return nil
}

func startGw(cfg *config.Server) {
	tls, _ := credentials.NewClientTLSFromFile(cfg.CertFile, "127.0.0.1")
	mux := runtime.NewServeMux()
	opts := []rpc.DialOption{
		rpc.WithTransportCredentials(tls),
	}
	err := grpc.RegisterClickTrafficHandlerFromEndpoint(context.Background(), mux, cfg.BindPort(), opts)
	if err != nil {
		log.Fatalf("%v\n", err)
	}
	log.Infof("starting http server")
	log.Fatal(http.ListenAndServe(":8080", mux))
}

func initLogger(level log.Level) {
	log.SetLevel(level)
}
