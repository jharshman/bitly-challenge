package server

import (
	"context"
	"github.com/grpc-ecosystem/go-grpc-middleware/auth"
)

type contextKey string

func (c contextKey) String() string {
	return string(c)
}

var contextKeyAuthToken = contextKey("auth-token")

func auth(ctx context.Context) (context.Context, error) {
	t, err := grpc_auth.AuthFromMD(ctx, "bearer")
	if err != nil {
		return nil, err
	}
	childCtx := context.WithValue(ctx, contextKeyAuthToken, t)
	return childCtx, nil
}
